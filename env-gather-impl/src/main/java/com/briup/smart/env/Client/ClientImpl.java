package com.briup.smart.env.Client;

import com.briup.smart.env.client.Client;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.util.Log;
import com.briup.smart.env.util.LogImpl;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collection;

public class ClientImpl implements Client {
    private Log log = new LogImpl();
    @Override
    public void send(Collection<Environment> c)  {
        //使用网络编程发送数据到服务端
        String host = "127.0.0.1";
        int port = 8888;
        Socket socket = null;
        ObjectOutputStream objectOutputStream = null;
        String encoding = "UTF-8";
        try {
            if (c==null|| c.size()==0 ){
                //System.out.println("接受数据有误");
                log.debug("接受数据有误");

                return;
            }
            socket = new Socket(host,port);
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            //System.out.println("客户端连接成功");
            log.fatal("客户端连接成功");
            //System.out.println("客户端准备发送数据");
            log.fatal("客户端准备发送数据");
            objectOutputStream.writeObject(c);
            //System.out.println("数据发送成功，共"+c.size()+"条");
            log.info("数据发送成功，共"+c.size()+"条");
            objectOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
                if (objectOutputStream!=null){
                    try {
                        objectOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (socket!=null){
                    try {
                        socket.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }

    }

