package com.briup.smart.env.Client;

import com.briup.smart.env.client.Gather;
import com.briup.smart.env.entity.Environment;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

public class Gatherimpl implements Gather {
    @Override
    public Collection<Environment> gather()  {
        String filePath ="C:\\Users\\tzx\\Desktop\\ppt\\data-file";
        ArrayList<Environment> list = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))){
            String str = null;
            while ((str = bufferedReader.readLine())!=null){
                //字符串分割,按照 | 分割
                String[] split = str.split("\\|");
                Environment environment = new Environment();
                //环境种类名称0
                //private String name;

                //发送端id1
                environment.setSrcId(split[0]);
                //树莓派系统id2
                environment.setDesId(split[1]);
                //实验箱区域模块id(1-8)3
                environment.setDevId(split[2]);
                //模块上传感器地址4
                environment.setSersorAddress(split[3]);
                //传感器个数5
                environment.setCount(Integer.parseInt(split[4]));
                //发送指令标号 3表示接收数据 16表示发送命令6
                environment.setCmd(split[5]);
                //状态 默认1表示成功 7
                environment.setStatus(Integer.parseInt(split[7]));
                //环境值8
                //environment.setData();
                //采集时间9
                environment.setGather_date(new Timestamp(Long.parseLong(split[8])));
                switch (split[3]){
                    case "16":
                        //表示温度则前两个字节是温度
                        environment.setName("温度");
                        String temperature = split[6].substring(0, 4);
                        int t = Integer.parseInt(temperature, 16);
                        environment.setData((t*(0.00268127F))-46.85F);
                        list.add(environment);
                        //表示湿度中间两个字节是湿度
                        Environment environment1 = new Environment();
                        environment1.setName("湿度");
                        String substring = split[6].substring(4, 8);
                        Integer.parseInt(substring,16);
                        environment1.setData((t*0.00190735F)-6);
                        //发送端id1
                        environment1.setSrcId(split[0]);
                        //树莓派系统id2
                        environment1.setDesId(split[1]);
                        //实验箱区域模块id(1-8)3
                        environment1.setDevId(split[2]);
                        //模块上传感器地址4
                        environment1.setSersorAddress(split[3]);
                        //传感器个数5
                        environment1.setCount(Integer.parseInt(split[4]));
                        //发送指令标号 3表示接收数据 16表示发送命令6
                        environment1.setCmd(split[5]);
                        //状态 默认1表示成功 7
                        environment1.setStatus(Integer.parseInt(split[7]));
                        //环境值8
                        //environment.setData();
                        //采集时间9
                        environment1.setGather_date(new Timestamp(Long.parseLong(split[8])));
                        list.add(environment1);
                        break;
                    case "256":
                        environment.setName("光照强度");
                        environment.setData(Integer.parseInt(split[6].substring(0,4),16));
                        list.add(environment);
                        break;
                    case "1280":
                        environment.setName("二氧化碳浓度");
                        environment.setData(Integer.parseInt(split[6].substring(0,4),16));
                        list.add(environment);
                        break;
                    default:
                        System.out.println("数据格式错误"+str);
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return list;
    }
}
