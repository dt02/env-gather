package com.briup.smart.env.main;

import com.briup.smart.env.Client.ClientImpl;
import com.briup.smart.env.Client.Gatherimpl;

/**
 * 客户端入口类
 */
public class ClientMain {
    public static void main(String[] args) throws Exception {
        new ClientImpl().send(new Gatherimpl().gather());
    }


}
