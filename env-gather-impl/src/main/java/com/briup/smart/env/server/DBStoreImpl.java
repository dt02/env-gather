package com.briup.smart.env.server;

import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.util.Backup;
import com.briup.smart.env.util.BackupImpl;
import com.briup.smart.env.util.Log;
import com.briup.smart.env.util.LogImpl;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DBStoreImpl implements DBStore {
    private Backup backup = new BackupImpl();
    private  String fileName= "D:\\briup_project\\env-gather\\env-gather-impl\\src\\main\\java\\com\\briup\\smart\\env\\util\\backups.txt";
    private Log log = new LogImpl();
    @Override
    public void saveDB(Collection<Environment> c) throws Exception {
        File file = new File(fileName);
        if (file.exists()&&file.length()>0){
            Object obj = backup.load(fileName, Backup.LOAD_REMOVE);
            if (obj instanceof Collection){
                Collection col = (Collection) obj;
                c.addAll(col);
                //System.out.println("入库模块：备份文件中的数据量"+col.size());
                log.debug("入库模块：备份文件中的数据量"+col.size());
                //System.out.println("入库模块：本次入库总数据量"+c.size());
                log.debug("入库模块：本次入库总数据量"+c.size());
            }
        }
        //入库模块服务端收到客户端发来的数据之后，
        //保存到数据库
        //通过jdbc写入到数据库
        String driverClass = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:XE";
        String user = "briup";
        String password = "briup";
        Connection conn = null;
        PreparedStatement ps = null;
        int saveCount = 0;
        //int testException = 0;//用于抛出异常
        try {
            //1.加载注册驱动
            Class.forName(driverClass);
            //2.获取连接对象
            conn = DriverManager.getConnection(url, user, password);
            //3.获取PS对象具体每次不同的数据，可以先用占位符符表示，ps会提前把sql发给数据库预处理，后面
            conn.setAutoCommit(false);
            int count = 0;//控制p处理执行
            int prepviousDay = -1;//表示前一条数据天数

            for (Environment environment:c){
                /*//手动抛出异常
                testException++;
                if (testException == 1000){
                    throw new RuntimeException("测试");
                }*/
                //获取日
                //Timestamp gather_date = environment.getGather_date();
                //int date = gather_date.getDate();已经被弃用
                //Calendar instance = Calendar.getInstance();
                //instance.setTimeInMillis(gather_date.getTime());
                //int currentDay = instance.get(Calendar.DAY_OF_MONTH);
                int day = Integer.parseInt(String.format("%td", environment.getGather_date()));
                if (prepviousDay!=day){
                    //前一条与这一条数据天数不同时创建新的ps对象
                    //根据拿到的天数拼接sql语句的表名
                    if (ps!=null) {
                        ps.executeBatch();
                        conn.commit();
                        saveCount+=count;
                        ps.close();
                        count = 0;
                    }
                    String sql = "insert into E_DETAIL_" + day +
                            " values (?,?,?,?,?,?,?,?,?,?)";
                    ps = conn.prepareStatement(sql);
                }
                //4.执行SQL语句
                ps.setString(1,environment.getName());
                ps.setString(2,environment.getSrcId());
                ps.setString(3,environment.getDesId());
                ps.setString(4,environment.getDevId());
                ps.setString(5,environment.getSersorAddress());
                ps.setInt(6,environment.getCount());
                ps.setString(7,environment.getCmd());
                ps.setInt(8,environment.getStatus());
                ps.setFloat(9,environment.getData());
                ps.setDate(10,new Date(environment.getGather_date().getTime()));
                ps.addBatch();
                count++;
                if(count == 500){
                    ps.executeBatch();
                    conn.commit();
                    saveCount+=count;
                    count = 0;

                }
                prepviousDay = day;
            }
            ps.executeBatch();
            conn.commit();
            saveCount+=count;
            ps.close();
            //5.处理结果
            //System.out.println("入库完成");
            log.fatal("入库完成");
            }
        catch (Exception e) {
            //事务回滚
            conn.rollback();
            //如果发生异常，存储未保存到数据库的数据到备份文件中。
            ArrayList<Environment> list = (ArrayList<Environment>) c;
            List<Environment> list2 = list.subList(saveCount, list.size());
            ArrayList<Environment> list3 = new ArrayList<>();
            list3.addAll(list2);
            backup.store(fileName,list3,backup.STORE_OVERRIDE);

            e.printStackTrace();
        }finally {
            //System.out.println("入库模块：已保存的数据"+(saveCount));
            log.debug("入库模块：已保存的数据"+(saveCount));
            //System.out.println("入库模块：未保存的数据"+(c.size()-saveCount));
            log.debug("入库模块：未保存的数据"+(c.size()-saveCount));
            //6.释放资源
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        e.printStackTrace();

                    }
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }






