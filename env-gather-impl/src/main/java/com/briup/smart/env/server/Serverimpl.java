package com.briup.smart.env.server;

import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.util.Log;
import com.briup.smart.env.util.LogImpl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;

public class Serverimpl implements Server{
    private boolean stop;
    private  ServerSocket serverSocket;
    private Log log =new LogImpl();
    @Override
    public void receiver() throws IOException {
        //接收客户端发送的数据，要一直接收
        new Thread(()->{
            try{
            serverSocket = new ServerSocket(8888);
            //System.out.println("服务端等待连接");
            log.fatal("服务端等待连接");
            while (!stop){
                Socket socket = serverSocket.accept();
                //System.out.println("连接成功");
                log.fatal("连接成功");
                new Thread(()->{
                    try (ObjectInputStream objectInputStream =
                                 new ObjectInputStream(socket.getInputStream())
                    ) {
                        //System.out.println("服务端准备接收数据");
                        log.fatal("服务端准备接收数据");
                        Object object = objectInputStream.readObject();
                        Collection<Environment> collection =
                                (Collection<Environment>) object;
                        //System.out.println("服务端接收数据完毕，共"+collection.size()+"条");
                        log.fatal("服务端接收数据完毕，共"+collection.size()+"条");
                        //System.out.println("存入到数据库中");
                        log.fatal("存入到数据库中");
                        //调用入库模块
                        try {
                            new DBStoreImpl().saveDB(collection);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }finally {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
            }catch(Exception e){
                e.printStackTrace();
                }
        }).start();

    }

    @Override
    public void shutdown() throws Exception {
        //负责关闭服务端，
        stop = true;
        if (serverSocket!=null){
            serverSocket.close();
        }

        //System.out.println("shutdown执行完毕");
        log.debug("shutdown执行完毕");

    }
}
