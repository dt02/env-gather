package com.briup.smart.env.util;

import org.apache.log4j.Logger;

import java.io.*;

public class BackupImpl implements Backup {
    /**
     * 读取备份文件,返回集合对象
     * fileName 存储位置
     * del 读取备份文件之后是否删除备份文件
     * 读取备份文件如果有数据和本次入库入库一起插入
     */
    private Log log = new LogImpl();
    @Override
    public Object load(String fileName, boolean del) throws Exception {
        File file = new File(fileName);
        if (!file.exists()) {
            //System.out.println("备份模块：想要读取的文件不存在" + fileName);
            log.debug("备份模块：想要读取的文件不存在" + fileName);
            return null;
        }
        Object object = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))) {
             object = objectInputStream.readObject();
            //System.out.println("备份模块：成功读取备份文件"+fileName);
            log.debug("备份模块：成功读取备份文件"+fileName);
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (del) {
            boolean delete = file.delete();
            //System.out.println("备份模1111块:" +(delete?"删除成功":"删除失败"));
            log.debug("备份模1111块:" +(delete?"删除成功":"删除失败"));
        }
       return object;
    }
        /**
         * 将需要备份的集合对象写入到备份文件
         * fileName 备份文件存储位置
         * obj 要保存的数据
         * append 保存时追加还是覆盖
         * 结合异常处理机制
         * 事务（cimmit,rollback）
         */
        @Override
        public void store (String fileName, Object obj,boolean append) throws Exception {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream(fileName, append))) {
                objectOutputStream.writeObject(obj);
                //System.out.println("备份模块已执行" + fileName);
                log.debug("备份模块已执行" + fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
